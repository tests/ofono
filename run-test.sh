#!/bin/sh
# vim: tw=0

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
. "${TESTDIR}/config.sh"

. "${TESTDIR}/common/update-test-path"

# We want to use the pre-existing session bus.
export RUN_AS_USER=no
export LAUNCH_DBUS="no"

error=0
"${TESTDIR}"/common/run-aa-test "${TESTDIR}"/ofonod.malicious.expected "${TESTDIR}"/ofonod malicious || error=$?
"${TESTDIR}"/common/run-aa-test "${TESTDIR}"/ofonod.normal.expected "${TESTDIR}"/ofonod normal || error=$?

if [ $error = 0 ]; then
  echo "# apparmor-ofono: all tests passed"
else
  echo "# apparmor-ofono: at least one test failed"
fi

exit $error
